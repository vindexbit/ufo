#!/bin/bash

BOLD='\e[1m'
LRED='\e[1;31m'
NORMAL='\e[0m'
LBLUE='\e[1;34m'
LGREEN='\e[92m'
UNDERLINED='\e[4m'
CYAN='\e[96m'

declare -i good_cases=0
declare -i bad_cases=0

CMD=""


failed() {
    bad_cases+=1
    echo -e "${LRED}× fail${NORMAL}"":" $CMD
    echo -e "$ERR"
}


passed() {
    good_cases+=1
    echo -e "  ${LBLUE}✓ pass${NORMAL}"":" $CMD
}


title() {
    echo -e "\n""${UNDERLINED}${BOLD}$@${NORMAL}"
}


procedure_title() {
    echo -e "\n>: ${BOLD}$@${NORMAL}\n"
}


case_title() {
    echo -e "  "${BOLD}${LGREEN}$@${NORMAL}
}


check_code_and_output() {
    if [[ $CODE -eq $EXP_CODE && "$OUTPUT" == "$EXP_OUTPUT" ]]; then
        passed
    else
        failed
        echo "OUTPUT >>" "$OUTPUT" "<<"
        echo "EXPECT >>" "$EXP_OUTPUT" "<<"
    fi
}

get_code_and_output() {
    OUTPUT=`$CMD 2>&1`
    CODE=$?
}

verify_code_and_output() {
    get_code_and_output
    check_code_and_output
}