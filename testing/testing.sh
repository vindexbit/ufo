#!/bin/bash
source def.sh
APP=./ufo

procedure01() {
    procedure_title "Procedure 01: non-existent files"

    case_title "One non-existent file"
    CMD="${APP} file.txt"
    EXP_OUTPUT="file.txt: file not found.
Incorrect use of ufo.
See: ufo --help"
    EXP_CODE=1
    verify_code_and_output

    case_title "Two non-existent files"
    CMD="${APP} file1.txt file2.txt"
    EXP_OUTPUT="file1.txt: file not found.
file2.txt: file not found.
Incorrect use of ufo.
See: ufo --help"
    EXP_CODE=1
    verify_code_and_output
}


procedure02() {
    procedure_title "Procedure 02: wrong arguments"

    case_title "No arguments"
    CMD="${APP}"
    EXP_OUTPUT="Incorrect use of ufo.
See: ufo --help"
    EXP_CODE=1
    verify_code_and_output

    case_title "Wrong option"
    CMD="${APP} --thing"
    EXP_OUTPUT="Incorrect use of ufo.
See: ufo --help"
    EXP_CODE=1
    verify_code_and_output

    case_title "Two wrong option"
    CMD="${APP} --thing --yes"
    EXP_OUTPUT="Incorrect use of ufo.
See: ufo --help"
    EXP_CODE=1
    verify_code_and_output

    case_title "Wrong option and non-existent file"
    CMD="${APP} --thing file.txt"
    EXP_OUTPUT="Incorrect use of ufo.
See: ufo --help"
    EXP_CODE=1
    verify_code_and_output

    case_title "Non-existent file and wrong options"
    CMD="${APP} file.txt --thing"
    EXP_OUTPUT="Incorrect use of ufo.
See: ufo --help"
    EXP_CODE=1
    verify_code_and_output
}

run_tests() {
    LANG=en_US.UTF-8
    procedure01
    procedure02
}
run_tests

echo -e "$BOLD$CYAN"
echo "Good cases: $good_cases"
echo "Bad cases:  $bad_cases"
echo -e "$NORMAL"

if [[ $bad_cases -ne 0 ]]; then
    exit 1
fi
