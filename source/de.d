/*
 * Copyright (C) 2018-2020 Eugene 'Vindex' Stulin
 * 
 * This file is part of ufo.
 * ufo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module de;

import std.algorithm,
       std.file,
       std.process,
       std.string;

enum DesktopEnvironment {
    Cinnamon,
    Enlightenment,
    GNOME,
    KDE,
    LXDE,
    LXQt,
    MATE,
    Pantheon,
    XFCE,
    None,
    Unknown
}
alias DE = DesktopEnvironment;
DE getCurrentDE() {
    DE de;
    string session;
    string envVar = "XDG_CURRENT_DESKTOP";
    session = environment.get(envVar, "");
    if (session == "") { //XDG_CURRENT_DESKTOP doesn't exist in Bodhi Linux
        string altEnvVar = "XDG_SESSION_DESKTOP";
        session = environment.get(altEnvVar, "");
    }
    session = session.toUpper;
    switch(session) {
        case "X-CINNAMON":    de = DE.Cinnamon;      break;
        case "ENLIGHTENMENT": de = DE.Enlightenment; break;
        case "GNOME":         de = DE.GNOME;         break;
        case "KDE":           de = DE.KDE;           break;
        case "LXDE":          de = DE.LXDE;          break;
        case "LXQT":          de = DE.LXQt;          break;
        case "MATE":          de = DE.MATE;          break;
        case "PANTHEON":      de = DE.Pantheon;      break;
        case "XFCE":          de = DE.XFCE;          break;
        case "":              de = DE.None;          break;
        default:
            //for "GNOME-Classic:GNOME", "ubuntu:GNOME" or "Budgie:GNOME"
            if (session.endsWith(":GNOME")) {
                de = DE.GNOME;
            } else {
                de = DE.Unknown;
            }
            break;
    }
    return de;
}

/*******************************************************************************
 * Returns opening application depending on the desktop environment
 */
string chooseOpenerByDefault() {
    immutable gnomeTool    = "gio open";
    immutable kdeTool      = "kde-open";
    immutable standartTool = "xdg-open";
    immutable consoleTool  = "mimeopen";

    DE desktopSession = getCurrentDE();
    string opener;

    if (desktopSession.among(
        DE.Cinnamon, DE.GNOME, DE.MATE, DE.Pantheon,
        DE.Enlightenment, DE.LXQt
    )) {
        opener = gnomeTool;
    } else if (desktopSession == DE.KDE) {
        opener = kdeTool;
    } else if (desktopSession == DE.None) {
        opener = consoleTool;
    } else {
        if (getAppPath(gnomeTool) != "") {
            opener = gnomeTool;
        } else if (getAppPath(kdeTool) != "") {
            opener = kdeTool;
        }
    }

    if (getAppPath(opener) == "") {
        opener = standartTool;
        if (getAppPath(opener) == "") {
            opener = consoleTool;        
            if (getAppPath(opener) == "") {
                opener = "";        
            }
        }    
    }

    return opener;
}


/*******************************************************************************
 * The function returns file path to application (searches in system catalogies)
 */
string getAppPath(string app) {
    string[] appsRootPaths = environment["PATH"].split(":");
    string appPath;
    foreach(p; appsRootPaths) {
        appPath = p ~ "/" ~ app;
        if (exists(appPath)) break;
    }
    return appPath;
}
