/*
 * Copyright (C) 2018-2021 Eugene 'Vindex' Stulin
 *
 * ufo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import core.stdc.stdlib;
import std.algorithm,
       std.array,
       std.file,
       std.path,
       std.process,
       std.stdio,
       std.string,
       std.typecons;
import de, langlocal;

immutable string app = "ufo";
immutable string appVersion = import("version").strip;
immutable string homeDir;
immutable string helpDir;
immutable translations = mixin(import("translations.dtxt").strip);
immutable string wrongUsingMsg;
immutable string hint;
immutable string mimeAppsList;
string[][string] mimeAppsSections;

shared static this() {
    string currentApp = readLink("/proc/self/exe");
    string shareDir = buildNormalizedPath(dirName(currentApp), "..", "share");
    helpDir = shareDir ~ "/help/";

    initLocalization(translations);
    wrongUsingMsg = "Incorrect use of ufo."._s;
    hint = "See: ufo --help"._s;

    homeDir = environment["HOME"];
    mimeAppsList = homeDir ~ "/.config/mimeapps.list";
    if (mimeAppsList.exists) {
        mimeAppsSections = getIniSections(mimeAppsList);
    }
}


void showHelp() {
    immutable lang = getCurrentLanguage();
    string pathToHelpFile = helpDir ~ lang ~ "/" ~ app ~ "/help.txt";
    if (!exists(pathToHelpFile)) {
        pathToHelpFile = helpDir ~ "en_US/" ~ app ~ "/help.txt";
    }
    writeln(readText(pathToHelpFile).strip);
}


void showVersion() {
    writeln(appVersion);
}


int main(string[] args) {
    if (args.length == 2 && args[1] == "--help") {
        showHelp();
        return 0;
    } else if (args.length == 2 && args[1] == "--version") {
        showVersion();
        return 0;
    }

    bool findAppByMimeType = true;
    bool findAppByFormat = false;
    foreach (i, arg; args) {
        if (arg == "-F" || arg == "--format") {
            findAppByMimeType = false;
            findAppByFormat = true;
            args.remove(i);
            break;
        }
    }
    foreach (arg; args) {
        if (arg.startsWith('-')) {
            stderr.writeln(wrongUsingMsg);
            stderr.writeln(hint);
            return 1;
        }
    }
    for (size_t i = 1; i < args.length; i++) {
        // check file and remove argument if it does not exist
        if (std.file.exists(args[i]) || args[i].canFind("://")) {
            continue;
        }
        stderr.writeln(args[i], ": file not found."._s);
        if (i != args.length - 1) {
            args = args[0 .. i] ~ args[i+1 .. $];
        } else {
            args = args[0 .. i];
        }
        i--;
    }
    if (args.length == 1) {
        stderr.writeln(wrongUsingMsg);
        stderr.writeln(hint);
        return 1;
    }
    auto files = args;
    if (findAppByMimeType) {
        return executeByMimeType(files);
    }
    return 1;
}


int executeByMimeType(string[] args) { 
    /*
    Additional information about field codes (%f, %F, %u, %U ...):
    https://standards.freedesktop.org/desktop-entry-spec/latest/ar01s07.html
    */

    string[][] rawCommands;   //commands with fields "%f", "%F"
    string[][] finalCommands; //prepared commands with opening files

    int exitCode = 0;

    foreach(i, f; args[1 .. $]) {
        string[] arguments = getCmdLineOptionsByFile(f);
        if (arguments.empty) {
            stderr.writeln(
              f ~ ": " ~
              "ufo could not determine the program to open the file."._s
            );
            exitCode = 2;
            continue;
        }
        exitCode = 0;
        if (!canFind(rawCommands, arguments)) {
            rawCommands ~= arguments;
            string[] newFinalCommand = arguments ~ f;
            finalCommands ~= newFinalCommand;
        } else {
            if (canFind(arguments, "%F")) {
                string program = arguments[0];
                foreach(ref finalCommand; finalCommands) {
                    auto currProgram = finalCommand[0];
                    if (currProgram == program) {
                        //expand file list for this program
                        finalCommand ~= f;
                        break;
                    }
                }
            } else {
                auto newFinalCommand = arguments ~ [f];
                finalCommands ~= newFinalCommand;
            }
        }
    }
    foreach(ref cmd; finalCommands) {
        cmd = std.algorithm.remove!(`a == "%F" || a == "%f"`)(cmd);
    }
    foreach(command; finalCommands) {
        writeln(command);
        spawnProcess(command);
    }
    return exitCode;
}


string[] getCmdLineOptionsByFile(string f) {
    string mimeType = getMimeType(f);
    string[] cmdOptions;
    if (!mimeType.empty) {
        string desktopFile = getAppDesktopFilesForMimeType(mimeType);
        if (!desktopFile.empty) {
            desktopFile = desktopFile.strip(';').split(";")[$-1];
            cmdOptions = getCmdLineOptionsByDesktopFile(desktopFile);
        }
    }
    if (cmdOptions.empty) {
        string opener = de.chooseOpenerByDefault();
        if (!opener.empty) {
            cmdOptions = opener.split.array ~ ["%f"];
        }
    }
    return cmdOptions;
}


string getMimeType(string filepath) {
    string cmd;
    cmd = `xdg-mime query filetype ` ~ `"` ~ filepath.replace(`"`, `\"`) ~ `"`;
    auto answer = executeShell(cmd);
    if (answer.status != 0) return "";
    else return strip(cast(string)(answer.output));
}


string getAppDesktopFilesForMimeType(string mimeType) {
    /*if (exists(mimeAppsList)) {
        auto mainSection = mimeAppsSections["Default Applications"];
        auto addedAssociations = mimeAppsSections["Added Associations"];

        auto beginOfMimeAppLine = mimeType ~ "=";
        foreach(line; mainSection) {
            if (line.startsWith(beginOfMimeAppLine)) {
                return line[beginOfMimeAppLine.length .. $].strip.idup;
            }
        }
        foreach(line; addedAssociations) {
            if (line.startsWith(beginOfMimeAppLine)) {
                return line[beginOfMimeAppLine.length .. $].strip.idup;
            }
        }
    }*/

    //old algorithm based on xdg-mime
    string cmd = `xdg-mime query default ` ~ `"`~mimeType~`"`;
    auto answer = executeShell(cmd);
    if (answer.status != 0) {
        return "";
    }
    return strip(cast(string)(answer.output));
}


auto getIniSections(string filepath) {
    string[][string] sections;
    auto text = readText(filepath);
    size_t[] indexesOfSectionBegins;
    string[] splittedText = text.splitLines;
    foreach(i, line; splittedText) {
        if (line.empty) {
            continue;
        } else if (line[0] == '[' && line[$-1] == ']') {
            indexesOfSectionBegins ~= i;
        }
    }
    foreach(i, index; indexesOfSectionBegins) {
        string sectionName = splittedText[index][1 .. $-1];
        string[] sectionLines;

        size_t lastLineNumber; // for current section
        if (i != indexesOfSectionBegins.length-1) {
            lastLineNumber = indexesOfSectionBegins[i+1] - 1;
        } else {
            lastLineNumber = splittedText.length - 1;
        }

        sectionLines = splittedText[index+1 .. lastLineNumber+1];
        sections[sectionName] = sectionLines;
    }
    return sections;
}


string[] getCmdLineOptionsByDesktopFile(string desktopFile) {
    desktopFile = "/usr/share/applications/" ~ desktopFile;
    if (!desktopFile.exists) {
    	return [];
    }
    string command;
    auto f = File(desktopFile, "r");
    foreach(line; f.byLine) {
        if (line.startsWith("Exec=")) {
            command = line["Exec=".length .. $].idup;
            break;
        }
    }

    string[] comArray = command.split(' ').array;
    string[] specFieldSet = ["%f", "%F", "%u", "%U"];
    foreach(ref arg; comArray) {
        //unknown argument (%i, %c or other)
        if (arg.startsWith("%") && !canFind(specFieldSet, arg)) arg = "";
        else if (arg == "%u") arg = "%f"; //for single file or URL
        else if (arg == "%U") arg = "%F"; //for multiple files of URLs
    }

    return comArray;
}
